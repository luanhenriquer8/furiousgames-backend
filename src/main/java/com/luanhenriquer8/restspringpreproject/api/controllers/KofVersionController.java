package com.luanhenriquer8.restspringpreproject.api.controllers;

import com.luanhenriquer8.restspringpreproject.api.dto.KofVersionsDTO;
import com.luanhenriquer8.restspringpreproject.api.services.KofVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/kof-versions")
public class KofVersionController {

    @Autowired
    private KofVersionService kofVersionService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<KofVersionsDTO> getAllKofVersions(){
        return kofVersionService.getAllKofVersions();
    }

    @ResponseBody
    @RequestMapping(value = "/{idKofVersion}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public KofVersionsDTO getKofVersion(@PathVariable Integer idKofVersion){
        return kofVersionService.getKofVersion(idKofVersion);
    }

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String createKofVersion(@RequestBody KofVersionsDTO kofVersionsDTO){
        kofVersionService.createKofVersion(kofVersionsDTO);
        return "Kof Version Saved Successfull";
    }

    @ResponseBody
    @RequestMapping(value = "/{idKofVersion}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteKofVersion(@PathVariable Integer idKofVersion){
        kofVersionService.deleteKofVersion(idKofVersion);
        return "Kof Version Deleted Successfull";
    }

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteKofVersion(){
        kofVersionService.deleteAllKofVersions();
        return "All Kof Version Deleted Successfull";
    }
}
