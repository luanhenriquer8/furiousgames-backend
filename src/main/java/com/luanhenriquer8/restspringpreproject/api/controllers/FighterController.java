package com.luanhenriquer8.restspringpreproject.api.controllers;

import com.luanhenriquer8.restspringpreproject.api.dto.FighterDTO;
import com.luanhenriquer8.restspringpreproject.api.entities.Fighter;
import com.luanhenriquer8.restspringpreproject.api.services.FighterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/fighter")
public class FighterController {

    @Autowired
    FighterService fighterService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FighterDTO> getAllFighters() {
        return fighterService.getAllPeople();
    }

    @ResponseBody
    @RequestMapping(value = "/{personId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FighterDTO getFighter(@PathVariable Integer idFighter) {
        return fighterService.getPerson(idFighter);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Fighter createFighter(@RequestBody FighterDTO fighterDTO) {
        return fighterService.createFighter(fighterDTO);
    }
}
