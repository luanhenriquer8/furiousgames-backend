package com.luanhenriquer8.restspringpreproject.api.config.security;

import java.io.Serializable;

public class AccessToken implements Serializable {

    private String token;

    public String token() {
        return token;
    }

    public void token(String token) {
        this.token = token;
    }
}
