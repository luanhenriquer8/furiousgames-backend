package com.luanhenriquer8.restspringpreproject.api.dto;

import com.luanhenriquer8.restspringpreproject.api.entities.BasicMovePK;
import com.luanhenriquer8.restspringpreproject.api.entities.Fighter;

public class BasicMovesDTO {

    private long idBasicMove;
    private String nameBasicMove;
    private String descriptionBasicMove;
    private long fkFighter;


    public long getIdBasicMove() {
        return idBasicMove;
    }

    public void setIdBasicMove(long idBasicMove) {
        this.idBasicMove = idBasicMove;
    }


    public String getNameBasicMove() {
        return nameBasicMove;
    }

    public void setNameBasicMove(String nameBasicMove) {
        this.nameBasicMove = nameBasicMove;
    }


    public String getDescriptionBasicMove() {
        return descriptionBasicMove;
    }

    public void setDescriptionBasicMove(String descriptionBasicMove) {
        this.descriptionBasicMove = descriptionBasicMove;
    }


    public long getFkFighter() {
        return fkFighter;
    }

    public void setFkFighter(long fkFighter) {
        this.fkFighter = fkFighter;
    }
}
