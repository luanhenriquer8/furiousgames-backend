package com.luanhenriquer8.restspringpreproject.api.dto;

import java.io.Serializable;

public class PersonDTO implements Serializable{



    private long personId;
    private String personName;


    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }


    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }
}
