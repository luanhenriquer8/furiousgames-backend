package com.luanhenriquer8.restspringpreproject.api.dto;

public class FighterDTO {

  private long idFighter;
  private String nameFighter;
  private long fkKofVersion;


  public long getIdFighter() {
    return idFighter;
  }

  public void setIdFighter(long idFighter) {
    this.idFighter = idFighter;
  }


  public String getNameFighter() {
    return nameFighter;
  }

  public void setNameFighter(String nameFighter) {
    this.nameFighter = nameFighter;
  }


  public long getFkKofVersion() {
    return fkKofVersion;
  }

  public void setFkKofVersion(long fkKofVersion) {
    this.fkKofVersion = fkKofVersion;
  }

}
