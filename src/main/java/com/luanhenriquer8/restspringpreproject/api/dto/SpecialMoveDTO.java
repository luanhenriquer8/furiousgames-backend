package com.luanhenriquer8.restspringpreproject.api.dto;

public class SpecialMoveDTO {

  private long idSpecialMove;
  private String nameSpecialMoves;
  private String descriptionSpecialMove;
  private long fkFighter;


  public long getIdSpecialMove() {
    return idSpecialMove;
  }

  public void setIdSpecialMove(long idSpecialMove) {
    this.idSpecialMove = idSpecialMove;
  }


  public String getNameSpecialMoves() {
    return nameSpecialMoves;
  }

  public void setNameSpecialMoves(String nameSpecialMoves) {
    this.nameSpecialMoves = nameSpecialMoves;
  }


  public String getDescriptionSpecialMove() {
    return descriptionSpecialMove;
  }

  public void setDescriptionSpecialMove(String descriptionSpecialMove) {
    this.descriptionSpecialMove = descriptionSpecialMove;
  }


  public long getFkFighter() {
    return fkFighter;
  }

  public void setFkFighter(long fkFighter) {
    this.fkFighter = fkFighter;
  }

}
