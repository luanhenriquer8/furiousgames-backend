package com.luanhenriquer8.restspringpreproject.api.dto.entity;

import java.io.Serializable;

public class BaseEntity implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = 4494298492839556227L;
}
