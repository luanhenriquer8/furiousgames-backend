package com.luanhenriquer8.restspringpreproject.api.dto;

public class KofVersionsDTO {

    private long idKofVersion;
    private String descriptionKofVersion;


    public long getIdKofVersion() {
        return idKofVersion;
    }

    public void setIdKofVersion(long idKofVersion) {
        this.idKofVersion = idKofVersion;
    }


    public String getDescriptionKofVersion() {
        return descriptionKofVersion;
    }

    public void setDescriptionKofVersion(String descriptionKofVersion) {
        this.descriptionKofVersion = descriptionKofVersion;
    }
}
