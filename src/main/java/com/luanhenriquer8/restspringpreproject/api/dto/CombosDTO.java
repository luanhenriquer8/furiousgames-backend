package com.luanhenriquer8.restspringpreproject.api.dto;

import com.luanhenriquer8.restspringpreproject.api.entities.ComboPK;
import com.luanhenriquer8.restspringpreproject.api.entities.Fighter;

public class CombosDTO {

    private long idCombo;
    private String nameCombo;
    private String descriptionCombo;
    private String hitsCombo;
    private long fkFighter;


    public long getIdCombo() {
        return idCombo;
    }

    public void setIdCombo(long idCombo) {
        this.idCombo = idCombo;
    }


    public String getNameCombo() {
        return nameCombo;
    }

    public void setNameCombo(String nameCombo) {
        this.nameCombo = nameCombo;
    }


    public String getDescriptionCombo() {
        return descriptionCombo;
    }

    public void setDescriptionCombo(String descriptionCombo) {
        this.descriptionCombo = descriptionCombo;
    }


    public String getHitsCombo() {
        return hitsCombo;
    }

    public void setHitsCombo(String hitsCombo) {
        this.hitsCombo = hitsCombo;
    }


    public long getFkFighter() {
        return fkFighter;
    }

    public void setFkFighter(long fkFighter) {
        this.fkFighter = fkFighter;
    }
}
