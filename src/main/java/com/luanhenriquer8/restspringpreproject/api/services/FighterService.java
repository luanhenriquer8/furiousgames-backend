package com.luanhenriquer8.restspringpreproject.api.services;

import com.luanhenriquer8.restspringpreproject.api.dto.FighterDTO;
import com.luanhenriquer8.restspringpreproject.api.entities.Fighter;

import java.util.List;

public interface FighterService {


    List<FighterDTO> getAllPeople();

    FighterDTO getPerson(Integer idFighter);

    Fighter createFighter(FighterDTO fighterDTO);
}
