package com.luanhenriquer8.restspringpreproject.api.services;

import com.luanhenriquer8.restspringpreproject.api.dto.PersonDTO;

import java.util.List;

public interface PersonService {


    List<PersonDTO> getAllPeople();

    PersonDTO getPerson(Integer personId);

    void createPerson(PersonDTO personDTO);

    void updatePerson(Integer personId, PersonDTO personDTO);

    Object deletePerson(Integer personId);
}
