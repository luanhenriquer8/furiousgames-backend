package com.luanhenriquer8.restspringpreproject.api.services;

import com.luanhenriquer8.restspringpreproject.api.dto.KofVersionsDTO;
import java.util.List;

public interface KofVersionService {


    List<KofVersionsDTO> getAllKofVersions();

    KofVersionsDTO getKofVersion(Integer idKofVersion);

    void createKofVersion(KofVersionsDTO kofVersionsDTO);

    void deleteKofVersion(Integer idKofVersion);

    void deleteAllKofVersions();
}
