package com.luanhenriquer8.restspringpreproject.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class RestSpringPreProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestSpringPreProjectApplication.class, args);
	}
}
