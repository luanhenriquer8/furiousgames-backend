package com.luanhenriquer8.restspringpreproject.api.repositories;

import com.luanhenriquer8.restspringpreproject.api.entities.KofVersion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KofVersionRepository extends JpaRepository<KofVersion, Integer> {
}
