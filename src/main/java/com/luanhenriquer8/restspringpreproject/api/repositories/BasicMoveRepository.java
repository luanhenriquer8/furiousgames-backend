package com.luanhenriquer8.restspringpreproject.api.repositories;

import com.luanhenriquer8.restspringpreproject.api.entities.BasicMove;
import com.luanhenriquer8.restspringpreproject.api.entities.BasicMovePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BasicMoveRepository extends JpaRepository<BasicMove, BasicMovePK> {
}
