package com.luanhenriquer8.restspringpreproject.api.repositories;

import com.luanhenriquer8.restspringpreproject.api.entities.SpecialMove;
import com.luanhenriquer8.restspringpreproject.api.entities.SpecialMovePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialMoveRepository extends JpaRepository<SpecialMove, SpecialMovePK> {
}
