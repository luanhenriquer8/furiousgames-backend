package com.luanhenriquer8.restspringpreproject.api.repositories;

import com.luanhenriquer8.restspringpreproject.api.entities.Fighter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FighterRepository extends JpaRepository<Fighter, Integer> {
}
