package com.luanhenriquer8.restspringpreproject.api.repositories;

import com.luanhenriquer8.restspringpreproject.api.entities.Combo;
import com.luanhenriquer8.restspringpreproject.api.entities.ComboPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComboRepository extends JpaRepository<Combo, ComboPK> {
}
