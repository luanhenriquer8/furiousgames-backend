package com.luanhenriquer8.restspringpreproject.api.serviceimpl;

import com.luanhenriquer8.restspringpreproject.api.dto.KofVersionsDTO;
import com.luanhenriquer8.restspringpreproject.api.entities.KofVersion;
import com.luanhenriquer8.restspringpreproject.api.repositories.KofVersionRepository;
import com.luanhenriquer8.restspringpreproject.api.services.KofVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KofVersionServiceImpl implements KofVersionService {

    @Autowired
    private KofVersionRepository kofVersionRepository;

    @Override
    public List<KofVersionsDTO> getAllKofVersions() {
        List<KofVersion> kofVersionsEntities = kofVersionRepository.findAll();
        List<KofVersionsDTO> kofVersionsDTOS = new ArrayList<>();

        for(KofVersion entity: kofVersionsEntities){
            KofVersionsDTO dto = new KofVersionsDTO();
            dto.setIdKofVersion(entity.getIdKofVersion());
            dto.setDescriptionKofVersion(entity.getDescriptionKofVersion());
            kofVersionsDTOS.add(dto);
        }

        return kofVersionsDTOS;
    }

    @Override
    public KofVersionsDTO getKofVersion(Integer idKofVersion) {
        KofVersion entity = kofVersionRepository.getOne(idKofVersion);
        KofVersionsDTO dto = new KofVersionsDTO();

        dto.setIdKofVersion(entity.getIdKofVersion());
        dto.setDescriptionKofVersion(entity.getDescriptionKofVersion());

        return dto;
    }

    @Override
    public void createKofVersion(KofVersionsDTO kofVersionsDTO) {
        KofVersion entity = new KofVersion();

        entity.setDescriptionKofVersion(kofVersionsDTO.getDescriptionKofVersion());

        kofVersionRepository.save(entity);


    }

    @Override
    public void deleteKofVersion(Integer idKofVersion) {
        kofVersionRepository.deleteById(idKofVersion);
    }

    @Override
    public void deleteAllKofVersions() {
        kofVersionRepository.deleteAll();
    }
}
