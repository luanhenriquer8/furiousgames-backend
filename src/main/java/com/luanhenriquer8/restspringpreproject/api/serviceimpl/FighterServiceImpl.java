package com.luanhenriquer8.restspringpreproject.api.serviceimpl;

import com.luanhenriquer8.restspringpreproject.api.dto.FighterDTO;
import com.luanhenriquer8.restspringpreproject.api.entities.Fighter;
import com.luanhenriquer8.restspringpreproject.api.repositories.FighterRepository;
import com.luanhenriquer8.restspringpreproject.api.services.FighterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FighterServiceImpl implements FighterService{

    @Autowired
    private FighterRepository fighterRepository;

    @Override
    public List<FighterDTO> getAllPeople(){
        List<Fighter> fighters = fighterRepository.findAll();
        List<FighterDTO> fighterDTOS = new ArrayList<>();

        for (Fighter entity: fighters){
            FighterDTO dto =  new FighterDTO();
            dto.setIdFighter(entity.getFighterPK().getIdFighter());
            dto.setNameFighter(entity.getNameFighter());
            dto.setFkKofVersion(entity.getKofVersion().getIdKofVersion());
        }
        return fighterDTOS;
    }

    @Override
    public FighterDTO getPerson(Integer idFighter) {
        Fighter entity = fighterRepository.getOne(idFighter);
        FighterDTO dto = new FighterDTO();

        dto.setIdFighter(entity.getFighterPK().getIdFighter());
        dto.setNameFighter(entity.getNameFighter());
        dto.setFkKofVersion(entity.getKofVersion().getIdKofVersion());

        return dto;
    }

    @Override
    public Fighter createFighter(FighterDTO fighterDTO) {
        Fighter fighter = new Fighter();
        // TODO create logic method
        return fighter;

    }
}
