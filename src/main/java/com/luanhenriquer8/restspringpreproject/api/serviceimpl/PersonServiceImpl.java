package com.luanhenriquer8.restspringpreproject.api.serviceimpl;

import com.luanhenriquer8.restspringpreproject.api.dto.PersonDTO;
import com.luanhenriquer8.restspringpreproject.api.entities.Person;
import com.luanhenriquer8.restspringpreproject.api.repositories.PersonRepository;
import com.luanhenriquer8.restspringpreproject.api.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public List<PersonDTO> getAllPeople() {

        List<Person> peopleListEntities = personRepository.findAll();
        List<PersonDTO> peopleListDTO = new ArrayList<>();

        for (Person entity : peopleListEntities) {
            PersonDTO personDTO = new PersonDTO();
            personDTO.setPersonId(entity.getPersonId());
            personDTO.setPersonName(entity.getPersonName());

            peopleListDTO.add(personDTO);
        }

        return peopleListDTO;
    }

    @Override
    public PersonDTO getPerson(Integer personId) {
        Person entity = personRepository.getOne(personId);
        PersonDTO personDTO = new PersonDTO();

        personDTO.setPersonId(entity.getPersonId());
        personDTO.setPersonName(entity.getPersonName());

        return personDTO;
    }

    @Override
    public void createPerson(PersonDTO personDTO) {
        Person person = new Person();

        person.setPersonName(personDTO.getPersonName());

        personRepository.save(person);
    }

    @Override
    public void updatePerson(Integer personId, PersonDTO personDTO) {
        Person person = personRepository.getOne(personId);
        Person personToUpdate = new Person();

        personToUpdate.setPersonId(person.getPersonId());
        personToUpdate.setPersonName(personDTO.getPersonName());

        personRepository.save(personToUpdate);
    }

    @Override
    public Object deletePerson(Integer personId) {
        personRepository.deleteById(personId);
        return "Example Controller Working";
    }
}
