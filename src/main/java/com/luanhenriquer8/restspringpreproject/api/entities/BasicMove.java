/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author luanhenriquer8
 */
@Entity
@Table(name = "basic_move")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "BasicMove.findAll", query = "SELECT b FROM BasicMove b")})
public class BasicMove implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected BasicMovePK basicMovePK;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name_basic_move")
    private String nameBasicMove;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "description_basic_move")
    private String descriptionBasicMove;

    @MapsId("idFighter")
    @ManyToOne(optional = false)
    @JoinColumns({
            @JoinColumn(name = "id_fighter", referencedColumnName = "id_fighter"),
            @JoinColumn(name = "fk_kof_version", referencedColumnName = "fk_kof_version")
    })
    private Fighter fighter;

    public BasicMove() {
    }

    public BasicMove(BasicMovePK basicMovePK) {
        this.basicMovePK = basicMovePK;
    }

    public BasicMove(BasicMovePK basicMovePK, String nameBasicMove, String descriptionBasicMove) {
        this.basicMovePK = basicMovePK;
        this.nameBasicMove = nameBasicMove;
        this.descriptionBasicMove = descriptionBasicMove;
    }

    public BasicMove(int idBasicMove, int fkFighter) {
        this.basicMovePK = new BasicMovePK(idBasicMove, fkFighter);
    }

    public BasicMovePK getBasicMovePK() {
        return basicMovePK;
    }

    public void setBasicMovePK(BasicMovePK basicMovePK) {
        this.basicMovePK = basicMovePK;
    }

    public String getNameBasicMove() {
        return nameBasicMove;
    }

    public void setNameBasicMove(String nameBasicMove) {
        this.nameBasicMove = nameBasicMove;
    }

    public String getDescriptionBasicMove() {
        return descriptionBasicMove;
    }

    public void setDescriptionBasicMove(String descriptionBasicMove) {
        this.descriptionBasicMove = descriptionBasicMove;
    }

    public Fighter getFighter() {
        return fighter;
    }

    public void setFighter(Fighter fighter) {
        this.fighter = fighter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (basicMovePK != null ? basicMovePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BasicMove)) {
            return false;
        }
        BasicMove other = (BasicMove) object;
        if ((this.basicMovePK == null && other.basicMovePK != null) || (this.basicMovePK != null && !this.basicMovePK.equals(other.basicMovePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.BasicMove[ basicMovePK=" + basicMovePK + " ]";
    }

}
