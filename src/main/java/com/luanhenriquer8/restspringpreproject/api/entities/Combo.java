/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luanhenriquer8
 */
@Entity
@Table(name = "combo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Combo.findAll", query = "SELECT c FROM Combo c")})
public class Combo implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected ComboPK comboPK;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name_combo")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String nameCombo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "description_combo")
    private String descriptionCombo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "hits_combo")
    private String hitsCombo;

//    @JoinColumn(name = "fk_fighter", referencedColumnName = "id_fighter", insertable = false, updatable = false)
    @MapsId("idFighter")
    @ManyToOne(optional = false)
    @JoinColumns({
            @JoinColumn(name="id_fighter", referencedColumnName="id_fighter"),
            @JoinColumn(name="fk_kof_version", referencedColumnName="fk_kof_version")
    })
    private Fighter fighter;

    public Combo() {
    }

    public Combo(ComboPK comboPK) {
        this.comboPK = comboPK;
    }

    public Combo(ComboPK comboPK, String nameCombo, String descriptionCombo, String hitsCombo) {
        this.comboPK = comboPK;
        this.nameCombo = nameCombo;
        this.descriptionCombo = descriptionCombo;
        this.hitsCombo = hitsCombo;
    }

    public Combo(int idCombo, int fkFighter) {
        this.comboPK = new ComboPK(idCombo, fkFighter);
    }

    public ComboPK getComboPK() {
        return comboPK;
    }

    public void setComboPK(ComboPK comboPK) {
        this.comboPK = comboPK;
    }

    public String getNameCombo() {
        return nameCombo;
    }

    public void setNameCombo(String nameCombo) {
        this.nameCombo = nameCombo;
    }

    public String getDescriptionCombo() {
        return descriptionCombo;
    }

    public void setDescriptionCombo(String descriptionCombo) {
        this.descriptionCombo = descriptionCombo;
    }

    public String getHitsCombo() {
        return hitsCombo;
    }

    public void setHitsCombo(String hitsCombo) {
        this.hitsCombo = hitsCombo;
    }

    public Fighter getFighter() {
        return fighter;
    }

    public void setFighter(Fighter fighter) {
        this.fighter = fighter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comboPK != null ? comboPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Combo)) {
            return false;
        }
        Combo other = (Combo) object;
        if ((this.comboPK == null && other.comboPK != null) || (this.comboPK != null && !this.comboPK.equals(other.comboPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.Combo[ comboPK=" + comboPK + " ]";
    }
    
}
