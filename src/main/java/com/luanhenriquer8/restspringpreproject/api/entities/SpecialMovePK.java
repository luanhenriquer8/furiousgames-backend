/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luanhenriquer8
 */
@Embeddable
public class SpecialMovePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_special_move")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idSpecialMove;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_fighter")
    private int fkFighter;

    public SpecialMovePK() {
    }

    public SpecialMovePK(int idSpecialMove, int fkFighter) {
        this.idSpecialMove = idSpecialMove;
        this.fkFighter = fkFighter;
    }

    public int getIdSpecialMove() {
        return idSpecialMove;
    }

    public void setIdSpecialMove(int idSpecialMove) {
        this.idSpecialMove = idSpecialMove;
    }

    public int getFkFighter() {
        return fkFighter;
    }

    public void setFkFighter(int fkFighter) {
        this.fkFighter = fkFighter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idSpecialMove;
        hash += (int) fkFighter;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SpecialMovePK)) {
            return false;
        }
        SpecialMovePK other = (SpecialMovePK) object;
        if (this.idSpecialMove != other.idSpecialMove) {
            return false;
        }
        if (this.fkFighter != other.fkFighter) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.SpecialMovePK[ idSpecialMove=" + idSpecialMove + ", fkFighter=" + fkFighter + " ]";
    }
    
}
