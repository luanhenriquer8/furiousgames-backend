/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luanhenriquer8
 */
@Embeddable
public class FighterPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_fighter")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idFighter;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_kof_version")
    private int fkKofVersion;

    public FighterPK() {
    }

    public FighterPK(int idFighter, int fkKofVersion) {
        this.idFighter = idFighter;
        this.fkKofVersion = fkKofVersion;
    }

    public int getIdFighter() {
        return idFighter;
    }

    public void setIdFighter(int idFighter) {
        this.idFighter = idFighter;
    }

    public int getFkKofVersion() {
        return fkKofVersion;
    }

    public void setFkKofVersion(int fkKofVersion) {
        this.fkKofVersion = fkKofVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idFighter;
        hash += (int) fkKofVersion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FighterPK)) {
            return false;
        }
        FighterPK other = (FighterPK) object;
        if (this.idFighter != other.idFighter) {
            return false;
        }
        if (this.fkKofVersion != other.fkKofVersion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.FighterPK[ idFighter=" + idFighter + ", fkKofVersion=" + fkKofVersion + " ]";
    }
    
}
