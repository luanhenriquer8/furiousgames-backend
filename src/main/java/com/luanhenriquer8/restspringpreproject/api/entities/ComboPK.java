/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luanhenriquer8
 */
@Embeddable
public class ComboPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_combo")
    private int idCombo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_fighter")
    private int fkFighter;

    public ComboPK() {
    }

    public ComboPK(int idCombo, int fkFighter) {
        this.idCombo = idCombo;
        this.fkFighter = fkFighter;
    }

    public int getIdCombo() {
        return idCombo;
    }

    public void setIdCombo(int idCombo) {
        this.idCombo = idCombo;
    }

    public int getFkFighter() {
        return fkFighter;
    }

    public void setFkFighter(int fkFighter) {
        this.fkFighter = fkFighter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCombo;
        hash += (int) fkFighter;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComboPK)) {
            return false;
        }
        ComboPK other = (ComboPK) object;
        if (this.idCombo != other.idCombo) {
            return false;
        }
        if (this.fkFighter != other.fkFighter) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.ComboPK[ idCombo=" + idCombo + ", fkFighter=" + fkFighter + " ]";
    }
    
}
