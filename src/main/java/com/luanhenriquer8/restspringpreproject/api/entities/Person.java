/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import com.luanhenriquer8.restspringpreproject.api.dto.PersonDTO;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luanhenriquer8
 */
@Entity
@Table(name = "person")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Person.findAll", query = "SELECT p FROM Person p")})
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "person_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer personId;

    @Size(max = 255)
    @Column(name = "person_name")
    private String personName;

    public Person() {
    }

    public Person(@NotNull Integer personId, @Size(max = 255) String personName) {
        this.personId = personId;
        this.personName = personName;
    }


    public Person(Integer personId) {
        this.personId = personId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personId != null ? personId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Person)) {
            return false;
        }
        Person other = (Person) object;
        if ((this.personId == null && other.personId != null) || (this.personId != null && !this.personId.equals(other.personId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.Person[ personId=" + personId + " ]";
    }




    /** This class is for to have a build pattern. It make easy to create a new object and
     * define its own fields. Following example how to use it:
     *
     * Person entity = new Person
     *      .PersonBuiler()
     *      .personId(1)
     *      .personName("name")
     *      .CreatePersonEntity();
     *
     * */
    public static class PersonBuiler{

        private Integer id;

        private String name;

        public PersonBuiler() {}

        public PersonBuiler id(Integer id){
            this.id = id;
            return this;
        }

        public PersonBuiler name(String name){
            this.name = name;
            return this;
        }

        public Person CreatePersonEntity(){
            return new Person(id, name);
        }
    }
}
