/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luanhenriquer8
 */
@Embeddable
public class BasicMovePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_basic_move")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idBasicMove;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_fighter")
    private int fkFighter;

    public BasicMovePK() {
    }

    public BasicMovePK(int idBasicMove, int fkFighter) {
        this.idBasicMove = idBasicMove;
        this.fkFighter = fkFighter;
    }

    public int getIdBasicMove() {
        return idBasicMove;
    }

    public void setIdBasicMove(int idBasicMove) {
        this.idBasicMove = idBasicMove;
    }

    public int getFkFighter() {
        return fkFighter;
    }

    public void setFkFighter(int fkFighter) {
        this.fkFighter = fkFighter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idBasicMove;
        hash += (int) fkFighter;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BasicMovePK)) {
            return false;
        }
        BasicMovePK other = (BasicMovePK) object;
        if (this.idBasicMove != other.idBasicMove) {
            return false;
        }
        if (this.fkFighter != other.fkFighter) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.BasicMovePK[ idBasicMove=" + idBasicMove + ", fkFighter=" + fkFighter + " ]";
    }
    
}
