/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author luanhenriquer8
 */
@Entity
@Table(name = "fighter")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Fighter.findAll", query = "SELECT f FROM Fighter f")})
public class Fighter implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected FighterPK fighterPK;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name_fighter")
    private String nameFighter;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fighter")
    private List<Combo> comboList;

    @JoinColumn(name = "fk_kof_version", referencedColumnName = "id_kof_version", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private KofVersion kofVersion;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fighter")
    private List<SpecialMove> specialMoveList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fighter")
    private List<BasicMove> basicMoveList;

    public Fighter() {
    }

    public Fighter(FighterPK fighterPK) {
        this.fighterPK = fighterPK;
    }

    public Fighter(FighterPK fighterPK, String nameFighter) {
        this.fighterPK = fighterPK;
        this.nameFighter = nameFighter;
    }

    public Fighter(int idFighter, int fkKofVersion) {
        this.fighterPK = new FighterPK(idFighter, fkKofVersion);
    }

    public FighterPK getFighterPK() {
        return fighterPK;
    }

    public void setFighterPK(FighterPK fighterPK) {
        this.fighterPK = fighterPK;
    }

    public String getNameFighter() {
        return nameFighter;
    }

    public void setNameFighter(String nameFighter) {
        this.nameFighter = nameFighter;
    }

    @XmlTransient
    public List<Combo> getComboList() {
        return comboList;
    }

    public void setComboList(List<Combo> comboList) {
        this.comboList = comboList;
    }

    public KofVersion getKofVersion() {
        return kofVersion;
    }

    public void setKofVersion(KofVersion kofVersion) {
        this.kofVersion = kofVersion;
    }

    @XmlTransient
    public List<SpecialMove> getSpecialMoveList() {
        return specialMoveList;
    }

    public void setSpecialMoveList(List<SpecialMove> specialMoveList) {
        this.specialMoveList = specialMoveList;
    }

    @XmlTransient
    public List<BasicMove> getBasicMoveList() {
        return basicMoveList;
    }

    public void setBasicMoveList(List<BasicMove> basicMoveList) {
        this.basicMoveList = basicMoveList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fighterPK != null ? fighterPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fighter)) {
            return false;
        }
        Fighter other = (Fighter) object;
        if ((this.fighterPK == null && other.fighterPK != null) || (this.fighterPK != null && !this.fighterPK.equals(other.fighterPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.Fighter[ fighterPK=" + fighterPK + " ]";
    }



}
