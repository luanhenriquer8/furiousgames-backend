/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luanhenriquer8
 */
@Entity
@Table(name = "kof_version")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KofVersion.findAll", query = "SELECT k FROM KofVersion k")})
public class KofVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_kof_version")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idKofVersion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "description_kof_version")
    private String descriptionKofVersion;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kofVersion")
    private List<Fighter> fighterList;

    public KofVersion() {
    }

    public KofVersion(Integer idKofVersion) {
        this.idKofVersion = idKofVersion;
    }

    public KofVersion(Integer idKofVersion, String descriptionKofVersion) {
        this.idKofVersion = idKofVersion;
        this.descriptionKofVersion = descriptionKofVersion;
    }

    public Integer getIdKofVersion() {
        return idKofVersion;
    }

    public void setIdKofVersion(Integer idKofVersion) {
        this.idKofVersion = idKofVersion;
    }

    public String getDescriptionKofVersion() {
        return descriptionKofVersion;
    }

    public void setDescriptionKofVersion(String descriptionKofVersion) {
        this.descriptionKofVersion = descriptionKofVersion;
    }

    @XmlTransient
    public List<Fighter> getFighterList() {
        return fighterList;
    }

    public void setFighterList(List<Fighter> fighterList) {
        this.fighterList = fighterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKofVersion != null ? idKofVersion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KofVersion)) {
            return false;
        }
        KofVersion other = (KofVersion) object;
        if ((this.idKofVersion == null && other.idKofVersion != null) || (this.idKofVersion != null && !this.idKofVersion.equals(other.idKofVersion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.KofVersion[ idKofVersion=" + idKofVersion + " ]";
    }
    
}
