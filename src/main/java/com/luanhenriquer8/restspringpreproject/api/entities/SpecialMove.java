/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luanhenriquer8.restspringpreproject.api.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luanhenriquer8
 */
@Entity
@Table(name = "special_move")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SpecialMove.findAll", query = "SELECT s FROM SpecialMove s")})
public class SpecialMove implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected SpecialMovePK specialMovePK;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name_special_moves")
    private String nameSpecialMoves;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "description_special_move")
    private String descriptionSpecialMove;

    @MapsId("idFighter")
    @ManyToOne(optional = false)
    @JoinColumns({
            @JoinColumn(name="id_fighter", referencedColumnName="id_fighter"),
            @JoinColumn(name="fk_kof_version", referencedColumnName="fk_kof_version")
    })
    private Fighter fighter;

    public SpecialMove() {
    }

    public SpecialMove(SpecialMovePK specialMovePK) {
        this.specialMovePK = specialMovePK;
    }

    public SpecialMove(SpecialMovePK specialMovePK, String nameSpecialMoves, String descriptionSpecialMove) {
        this.specialMovePK = specialMovePK;
        this.nameSpecialMoves = nameSpecialMoves;
        this.descriptionSpecialMove = descriptionSpecialMove;
    }

    public SpecialMove(int idSpecialMove, int fkFighter) {
        this.specialMovePK = new SpecialMovePK(idSpecialMove, fkFighter);
    }

    public SpecialMovePK getSpecialMovePK() {
        return specialMovePK;
    }

    public void setSpecialMovePK(SpecialMovePK specialMovePK) {
        this.specialMovePK = specialMovePK;
    }

    public String getNameSpecialMoves() {
        return nameSpecialMoves;
    }

    public void setNameSpecialMoves(String nameSpecialMoves) {
        this.nameSpecialMoves = nameSpecialMoves;
    }

    public String getDescriptionSpecialMove() {
        return descriptionSpecialMove;
    }

    public void setDescriptionSpecialMove(String descriptionSpecialMove) {
        this.descriptionSpecialMove = descriptionSpecialMove;
    }

    public Fighter getFighter() {
        return fighter;
    }

    public void setFighter(Fighter fighter) {
        this.fighter = fighter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (specialMovePK != null ? specialMovePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SpecialMove)) {
            return false;
        }
        SpecialMove other = (SpecialMove) object;
        if ((this.specialMovePK == null && other.specialMovePK != null) || (this.specialMovePK != null && !this.specialMovePK.equals(other.specialMovePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "f.SpecialMove[ specialMovePK=" + specialMovePK + " ]";
    }
    
}
